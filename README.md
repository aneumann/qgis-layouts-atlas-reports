# qgis-layouts-atlas-reports

# Workshop "Tips and Tricks for QGIS layouts, reports and atlas serial printing"
## FOSS4G 2019 (Bucharest)

This is an advanced workshop that assumes that you already know the basics of QGIS
layouts (e.g. how to create and modify objects on a layout), variables and expressions.

## Workshop topics

* General layout topics
    - Dynamically placed elements in a layout that adapt to page size/orientation
    - Display information on project, software, date and user
    - Adding a north arrow
    - Adding a dynamic scale bar
    - Adding gridlines
    - Display information on the rendered map and layers
* Atlas topics
    - How to create an atlas by example of Swiss cantons
    - Display attribute values of atlas feature
    - Using dynamic images in an atlas
    - Styling of atlas features
    - Using rounded map scales in an atlas
    - Adding a linked overview map
    - Adding a table of related features from another layer
    - Adding page numbers
* Reports
    - what are reports?
    - The report organizer
    - Linking child group sections with their parent group section
    - Report limitations

## General layout topics
### Dynamically placed elements in a layout

First, please add a new map item to your new empty layout. Go into the item
properties of the newly added item and assign a new id value called "main_map":

![Screenshot assign item id](./screenshots/assign_item_id.png "Screenshot assign item id")

This helps us to later reference this particular map in order to query information
in expressions about this item.

If you want a layout adapt to different page sizes, there is a way to use
data defined properties for position and size of elements in a layout.

We need to create new variables, e.g. on the project level scope:

Open Menu "Project" --> Tab "Variables" --> Section "Project" and add two new
variables:

* layout_margin with a value of perhaps "10"
* layout_top_margin with a value of perhaps "50"

![Screenshot project variables](./screenshots/layout_variables_in_project-scope.png "Screenshot project variables")

In addition to the manually created two variables there are automatically provided
variables available, such as:

* layout_width
* layout_height
* layout_dpi
* layout_name
* layout_page (Current page number in composition)
* layout_numpages (Number of pages in composition)

Then you can create and place new items in the layouts and adjust their position
and size based on these variables either directly or based on an expression using these
variables.

![Screenshot data-defined position and size](./screenshots/data-defined-position-and-size.png "Screenshot data-defined position and size")

![Screenshot expressions for data-defined size](./screenshots/expressions-for-size.png "Screenshot expression for data-defined size")

Try changing the page size or orientation by right clicking somewhere in the
layout page rectangle:

![Screenshot changing page size or orientation](./screenshots/changing_page_size_and_orientation.png "Screenshot changing page size or orientation")

After changing the page size or orientation, the map (or any other dynamically positioned
and sized) item should automatically adjust to the new page size or orientation.
You might have to refresh the layout to get rid of rendering artefacts.


### Display information on project, map layers and user
Make sure you added a project title in your project properties. See menu
"Project" --> "Properties" --> Tab "General" --> "Project Title":

![Screenshot setting project title](./screenshots/project_title.png "Screenshot setting project title")

Add a new text label in your layout and add a new expression with the variable
@project_title:

![Screenshot project title in layout](./screenshots/project_title_in_layout.png "Screenshot project title in layout")


Create or edit a new project variable called "orgname" and provide a meaningful
value (such as "QGIS user group Switzerland"). Add a new text label below the
label of the project title and add the label text "provided by [% @orgname %]":

![Screenshot provided by @orgname](./screenshots/provided_by_orgname.png "Screenshot provided by @orgname")

We also want to inform the user when this map was printed, in which projection, by who and by what
software and platform:

![Screenshot map printed by](./screenshots/map_printed_by.png "Screenshot map printed by")

Here is the full text of the label:

`This map was printed on [% format_date( $now , 'yyyy-MM-dd')%] in the [%map_get(item_variables('main_map'),'map_crs_description')%] projection by [% @user_full_name %] using QGIS [% @qgis_short_version %] [% title(@qgis_platform)%] on [%title(@qgis_os_name)%]`

There are a number of interesting variables and expressions in this label:

* `format_date( $now , 'yyyy-MM-dd')` - for printing today's date
* `map_get(item_variables('main_map'),'map_crs_description')` - for getting the CRS description of the current map
* `@user_full_name` - for exposing the full user name logged into the operating system
* `@qgis_short_version` - for showing the short version string
* `@qgis_platform` - "desktop" or "server"
* `@qgis_os_name` - "linux", "windows" or "osx"
* `title(string)` - for capitalizing the first letter of each word

### Adding a north arrow

If your QGIS version is 3.8 or higher you can use the new "North arrow" item which is
a shortcut for placing an image and linking it with the closest map on the layout. If your
QGIS version is 3.6 or lower you have to use the "Image" item and link it with the
correct map frame for automatic rotation.

![Screenshot adding north arrow](./screenshots/adding_north_arrow.png "Screenshot adding north arrow")

When adding the north arrow item, make sure you consider the following:

* Use "Zoom and Resize Frame" as a Resize mode for the north arrow image
* In our layout, use "top - right" as the reference point
* Add a variable or expression for x and y for dynamic positioning
* Make sure the north arrow is linked with the correct map

Test the linking between map rotation and north-arrow rotation by setting a value in
the "Main Properties" --> "Map rotation" setting.
 
### Adding a dynamic scale bar

When adding a scalebar you should consider at least the following issues:

* Choosing a style
* Choose "Fit segment width" and choose the number of segments and min/max segment sizes.
This will always choose a round number for the segment size in km and make sure
that the scalebar doesn't grow or shrink too much.
* Choose "bottom right" as the reference point for the scale bar and set 
variables / expressions for x and y positioning. This ensures the scalebar is always
right aligned. In our case we used and `@layout_pagewidth - @layout_margin` for
x-positioning and `@layout_top_margin - 2.5` for y-positioning.

![Screenshot adding a scalebar](./screenshots/adding_scalebar.png "Screenshot adding a scalebar")

### Adding grid lines

One can add multiple grid lines (either with grid lines or crosses) as overlays above
a map item. Each grid can represent a different coordinate system. E.g. you could have a grid
in the Swiss national coordinate system and a second one in a different color with a different
lat/lon based coordinate system. For each grid you can decide if you want to show on which side (left / right)
or top/bottom you want to show and label it.

![Screenshot adding a grid](./screenshots/adding_a_grid.png "Screenshot adding a grid")

There are a lot of properties which we don't cover in detail here and which should be obvious what they mean.

![Screenshot grid properties](./screenshots/grid_properties.png "Screenshot grid properties")

One thing good to know is the custom format for the grid labels. You can use something like the following expression
to also show thousand separators:

```
format_number(@grid_number,0)
```

### Adding the layer name to the map title

We want to display what level of administrative units is actually displayed in the map. By using the
"mutually exclusive" property of a group we can make sure that only one of the layers "Cantons",
"Districts" or "Municipalities" is displayed:

![Screenshot Mutually Exclusive Group](./screenshots/mutually_exclusive.png "Screenshot Mutually Exclusive Group")

With the following expression we can find out which layers are active in our main_map in the 
current layout:

```
array_to_string(
	array_foreach(
		map_get(item_variables('main_map'),'map_layers'),
		layer_property(@element,'name')
	)
)
```

The layer list is always returned in the renderin order. Because we are only interested
in the first layer, and not in the background map layers, we can use the
following expression to only get the first layer of the mutually exclusive group:

```
array_first(
	array_foreach(
		map_get(item_variables('main_map'),'map_layers'),
		layer_property(@element,'name')
	)
)
```

And we can add this information to our map title as follows:

![Screenshot extending layer title with layer name](./screenshots/extending_layer_title_with_layer_name.png "Screenshot extending layer title with layer name")

### Adding the number of features in a layer to a text label

In a separate text label we want to show how many features are available in the
chosen layer with administrative units. We can use the following epxression for that:

```
array_first(
	array_foreach(
		map_get(item_variables('main_map'),'map_layers'),
		layer_property(@element,'feature_count')
	)
)
```

in order to get the following label:

![Screenshot display nr of features in layer](./screenshots/nr_of_features_in_layer.png "Screenshot display nr of features in layer")

## Atlas topics
### How to create an atlas by example of Swiss cantons

In order to create an Atlas layout you can start with a copy of the dynamic layout from
the previous exercise. Just duplicate the existing layout and start from that copy.

First you have to start with the general "Atlas Settings" which appear in a separate
panel next to the "Layout", "Item properties" or "Items" panel. You'll have to do the following steps for the setup:

1.  Activate the "Generate an Atlas" Checkbox
2.  Choose "Cantons" as the coverage layer
3.  Select the "name" attribute as the page  name
4.  Activate the "Sort by" checkbox and select "name" as the sort attribute

![Screenshot general atlas settings](./screenshots/general_atlas_settings.png "Screenshot general atlas settings")

Then you need to select the "main_map" map and go to the "Item properties" and "Controlled by Atlas" section:

5.  Activate the "Controlled by atlas" checkbox
6.  Select Margin around feature radio button with 10% of margin

![Screenshot controlled by atlas settings](./screenshots/controlled_by_atlas_setting.png "Screenshot controlled by atlas settings")

Now you can already activate the atlas preview in the "Atlas" toolbar and select a specific Canton to display or
alternatively step through each instance. The atlas should center the selected Canton in the
main map.

### Display attribute values of atlas feature

We want to display the name of the Canton in the title. As soon as an atlas is configured
and active one can access the "Fields and Values" of the atlas feature in the expression editor:

![Screenshot atlas fields and values](./screenshots/atlas_fields_and_values.png "Screenshot atlas fields and values")

and you can display the field values like in the following screenshot:

![Screenshot field values in label](./screenshots/canton_in_title.png "Screenshot field values in label")

We also want to create a new text label containing an HTML table displaying some dynamic
attributes of the atlas feature. We can use the following content with field values mixed in:

```html
<table style="background-color:lightgray;">
<tr>
	<td width="250">Name: [%name%]</td>
	<td width="250">Abbreviation: [%abbreviation%]</td>
</tr>
<tr style="background-color:white;">
	<td>Languages spoken: [%languages%]</td>
	<td>Population: [%format_number(population,0)%]</td>
</tr>
<tr>
	<td>Area: [%format_number(area,0)%] km<sup>2</sup></td>
	<td>Lake area: [%COALESCE(format_number(lake_area,0),0)%] km<sup>2</sup></td>
</tr>
</table>
```

For proper display of html code you need to enable the "Reder as HTML" checkbox:

![Screenshot render atlas field values in html table](./screenshots/atlas_table_as_html.png "Screenshot render atlas field values in html table")

### Using dynamic images in an atlas

Place a new image item and set the following expression as data-defined image source path:

```
'/local/path/to/repo/qgis-layouts-atlas-reports/swiss-base-data/coat_of_arms/' || lower(abbreviation) || '.svg'
```

Replace the local path with your local absolute path. Unfortunately, relative paths to your project isn't supported (yet).

For the image placement please use "Top-Right" anchor points, use resize mode "zoom", "Top-Right" as reference point,
position "x" to `@layout_pagewidth - @layout_margin`, "y" to `@layout_margin`

![Screenshot dynamic image placment in atlas layout](./screenshots/atlas_dynamic_image_placement.png "Screenshot dynamic image placment in atlas layout")

### Styling of atlas features

Often it is useful to style or label the atlas feature different from the rest of the features in the atlas coverage layer.
For this purpose it is useful to use the "rule based renderer" and add a separate rule to style
only the atlas feature, e.g. for highlighting. In our case we render the atlas feature twice:
first with a fill and then above it again on with a red-stroke on top of all the other features.
The expression to use in the rule is as follows:

```
$id = @atlas_featureid
```

![Screenshot separate symbology rule for atlas feature](./screenshots/atlas_style_by_atlas-id.png "Screenshot separate symbology rule for atlas feature")

After creating this rule, please enable symbol levels and render all other features in
the first rendering step (0) and the atlas feature in the second step (1):

![Screenshot enable symbol levels](./screenshots/enable_symbol_levels.png "Screenshot enable symbol levels")

![Screenshot atlas feature symbol level configuration](./screenshots/symbol_levels_for_atlas.png "Screenshot atlas feature symbol level configuration")


### Using round map scales

In some scenarios you might want to enforce round map scales instead of unround ones which
results if you use the 10% margin around an atlas feature.

We can define a list of predefined map scales either on the global level (menu "Settings"
&rarr; "Options" &rarr; Tab "Map Tools" &rarr; section "Predefined Scales") or
on the project level (menu "Project" &rarr; "Properties" &rarr; Tab "General" &rarr; Section "Project Predefined Scales").
In our project we used the project level approach. Here you can add / remove predefined
map scales or export / import them to / from an xml-file, as provided in this repo
in the file "map_scales.xml".

![Screenshot project predefined scales](./screenshots/project_defined_map-scales.png "Screenshot project predefined scales")

We are now replacing the graphical scalebar with a numerical one. Although there is a numeric
style available for the scalebar, we are preferring a text label with an expression, since we can better add prefixes and postfixes
in a text label. We can use the following content with expression:

```
Scale  1 : [%format_number(
	map_get(
		item_variables('main_map'),
		'map_scale'
   ),
   0
)%]
```

![Screenshot label with numeric scale](./screenshots/label_with_scale.png "Screenshot label with numeric scale")

Finally, we need to adjust the atlas setting of the main map to use rounded scales.
Select the main map in the layout, go to "Item properties" &rarr; Section "Controlled by atlas"
and switch to "Predefined Scale (Best Fit)".

![Screenshot use predefined scale in main map](./screenshots/atlas_use_predefined_scale_best-fit.png "Screenshot use predefined scale in main map")


Confirm if the setting works by enabling the atlas preview and browse through the
individual features.

### Adding a linked overview map

Often it is valuable to show the spatial context of the current map extent in the
main map. A linked overview map is a good way to achieve that. Please add a second, small
map and select the "Overview Map" as a map theme for the small overview map:

![Screenshot linking map theme with small overview map](./screenshots/overview_map_linking_to_map-theme.png "Screenshot linking map theme with small overview map")

This results in only the green country border to be displayed in the small overview map.

In the next step we need to establish a link between the large main map and the small
overview map. An overview map can show the extents of more than one map with different styles.
Go to the "Overviews" section of the "Item properties" of the small map and use the button
with the green "plus" to add a new overview map:

![Screenshot linking main map with overview map](./screenshots/overview_map_settings.png "Screenshot linking main map with overview map")

 Here we need to select the map id of the main map from the combobox and we should
 immediately see a small rectangle representing the current map extent from the main map.
 We can change the style and the position of this rectangle (below map, above labels or
 above/below a specific map layer.)
 
 Finally, we want to modify the symbology of this small rectangle representing the extent
 of the main map. In addition to the already existing symbol layer, we add a new centroid fill layer with a red
 cross symbol as a marker. This marker is still clearly visible if the rectange gets
 too small to be properly depicted.
 
 ![Screenshot changing symbology of rectangle representing extent of main map](./screenshots/overview_map_styles.png "Screenshot changing symbology of rectangle representing extent of main map")

### Adding a table of related features from another layer

We want to add a table of the related table "Municipalities" to show all municipalities of the
currently active canton. The table relations are already defined in our project and can be found in the 
main application window in menu "Project" &rarr; "Properties" &rarr; "Relations". Relations need one or more common key between
the two tables, e.g. a foreign key of one table linking to the primary key of another table. The relation
strength is either "Association" or "Composition", the latter meaning that a feature in the child table pointing to a
feature in the parent table is automatically removed if the parent feature is deleted.

![Screenshot table relations in project properties](./screenshots/table_relations.png "Screenshot table relations in project properties")

We add a second page to the layout by using the menu "Layout" &rarr; "Add pages". We change the page
orientation to "Portrait" by right-clicking in the page and then changing the page orientation.

On this second page we add a new "table item" by drawing a new frame that covers most of the page
minus some space on the top for a header label. Above the table we add a new label with the following expression
as label content:

```
Municipalities of Canton [% name %]
```
Next, we switch back to the frame of the attribute table and set a number of properties:

1. Change the Source to "Relation Children" and select the already defined relation "CantonsMunicipalities"
2. Open the "Attributes" dialogue (separate screenshot below): remove columns you don't need and change headers of columns to a more "human friendly" version and add a sort order
3. Increase the maximum nr of rows to something like 400 (the maximum expected nr of rows in one canton)
4. Add a filter `population IS NOT NULL` in order to remove lake features who aren't really municipalities
5. Change the appearance (separate screenshot below)
6. Change Frame settings: Switch to "Extend to next page" and enable the two checkboxes "Don't export page if frame is empty" and "Don't draw background if frame is empty"

![Screenshot general attribute table settings](./screenshots/attribute_table_general_settings.png "Screenshot general attribute table settings")

Regarding the management of attribute columns, you can remove columns by using the "Minus" button, add columns or Expressions using the "Plus" button,
change the column header, column alignment and width and one or more columns for sorting the table. See Screenshot below

![Screenshot attribute table managing attribute columns](./screenshots/attribute_table_managing_columns.png "Screenshot attribute table managing attribute columns")

Regarding table appearance we change the following:

1. Open the dialogue for "Advance Customization"
2. Change backgrounds for even and odd rows, as well as header rows
3. Change font properties for table header rows
4. Change font properties for table content rows

See Screenshot below for table appearance settings:

![Screenshot attribute table appearance and styling](./screenshots/attribute_table_styling_options.png "Screenshot attribute table appearance and styling")

CURRENT ISSUE: table frames on subsequent pages unfortunately do not honor the y-offset and height of the table frame as 
we defined it on the first page ;-( hopefully will be fixed in an upcomgin version ...

### Adding page numbers

For layouts with a static number of pages (e.g. 3 pages per atlas feature) you can use the following expression
to display page numbers:

```
[% @layout_numpages * @atlas_featurenumber + @layout_page - @layout_numpages %]
```

For layouts with dynamic number of pages, like in our example, there is unfortunately currently no way
to place proper page numbers. The best we can do is just display the @atlas_featurenumber in relation to
the total number of atlas feature. This number is not a real page number but shows the nth
nr of atlas features in relation to the total nr of features in the atlas coverage layer:

```
Canton [% @atlas_featurenumber %] / [% @atlas_totalfeatures %]
```

## Reports

### What are reports?

[Blog post Nyall Dawson as an introduction to reports](https://north-road.com/2018/01/23/exploring-reports-in-qgis-3-0-the-ultimate-guide/)

Reports can be added from the main application "Project" menu &rarr; "New Report".

Reports are composed of sections. Each section optionally consists of:

*  a header
*  a body
*  a footer

Sections can be either of type

* Static Layout Section (Just one static page)
* Field Group Section (Iterating over all filtered features of a layer)

In the future, more section types can be added.

### The report organizer

![Screenshot report organizer](./screenshots/report_organizer.png "Screenshot report organizer")

The report organizer helps you to organize the above mentioned nested sections.

It allows to add or remove a subgroup, show or hide headers and select layout pages (in header|body|footer)
to be selected for edition. In edition, it allows to set the coverage layer, key field and
sorting.


### Linking child group sections with their parent group section

Linking a child field group section to their parent group section requires the child table
to have an attribute with *exactly* the same name and type as the selected key attribute
defined in the parent field group section.

An example:

If your parent field group section layer with title "Cantons" has the "kantonsname" as the
key attribute, your child field group section layer needs to have an attribute named "kantonsname"
as well, as this is the common key assumed by the report generator. Currently, it is not possible
to use already defined relations on the project level, or to set up a relation where the columen names
do not match.

### Report limitations

* Reports don't have a preview
* Linking Groups for filtering child according to parent data can only be done if
child table contains an attribute column with exactly the same name as indicated in the
above parent group. Relations on the project levels are not yet supported.
* Additional filters on reports are not possible yet
* Objects that flow across multiple pages can still be a pain (same in normal layout and atlas)